package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;

@RestController
public class SimpleController {

    @RequestMapping("/sample")
    SampleModel sample() {
        SampleModel sampleModel = new SampleModel();
        sampleModel.setDate(LocalDate.now());
        sampleModel.setTime(LocalTime.now());
        return sampleModel;
    }
}

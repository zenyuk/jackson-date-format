package com.example;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class SampleModel {
    @JsonFormat( pattern="MM/dd/yyyy")
    private LocalDate date;

    @JsonFormat( pattern="HH:mm")
    private LocalTime time;
}

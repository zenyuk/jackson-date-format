package com.example;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.text.SimpleDateFormat;


@SpringBootApplication
public class DemoApplication {

//	@Configuration
//	public class JacksonConfig {
//
//		@Bean
//		@Primary
//		public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
//			ObjectMapper objectMapper = builder.createXmlMapper(false).build();
//			objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//			return objectMapper;
//		}
//	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		System.out.println("done");
	}
}
